
let alumno =[{ "matricula":"2020030714",
"nombre":"Ruiz Guerrero Axel Jovani",
"grupo":"7-3",
"carrera":"Tecnologias de la Informacion",
"foto":"/img/jovani.jfif"},{"matricula":"2021030136",
"nombre":"Oscar Alejandro Solis Velarde",
"grupo":"7-3",
"carrera":"Tecnologias de la Informacion",
"foto":"/img/yo.jfif"},{"matricula":"2021030097",
"nombre" : "Martinez Ortiz Axel Jehozadaq",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/genshin.jfif"},{"matricula":"2021030142",  
"nombre" : "Garcia Gonzalez Jorge Enrique",     
"grupo" : "7-3",     
"carrera" : "Tecnologias de la informacion",     
"foto" : "/img/postu.jfif"},{ "matricula":"2020030321",
"nombre" : "Ontiveros Govea Yair Alejandro ",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/govea.jfif"},{"matricula":"2021030262",
"nombre" : "Qui Mora Ángel Ernesto",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/qui.jfif"},{"matricula":"2021030314",
"nombre" : "Felipe Andrés Peñaloza Pizarro",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/peñaloza.jpg"},{"matricula":"2015030311",
"nombre" : "Reyes Lizarraga Jonathan Alexis",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/wero2.jfif"},{"matricula":"2019030880",
"nombre" : "Quezada Ramos Julio Emiliano",
"grupo" : "7-3",
"carrera" : "Tecnologias de la informacion",
"foto" : "/img/julio.jfif"},
{
    "matricula": "mosuna",
    "nombre": "Melissa Osuna Cárdenas",
    "grupo": "7-3 - Tutora",
    "carrera": "Tecnologías de la Información",
    "foto": "/img/mosuna.jfif"
} ] 
    

document.addEventListener("DOMContentLoaded", function () {
    const lista = document.getElementById("listalu");

    alumno.forEach((alumno) => {
        const div = document.createElement("div");
        div.className = "alumno";
        div.innerHTML = `
            <img src="${alumno.foto}" alt="${alumno.nombre}">
            <h2>${alumno.nombre}</h2>
            <br>
            <p>Matrícula: <br> ${alumno.matricula}</p>
            <p>Carrera:<br> ${alumno.carrera}</p>
            <br>
            <p>Grupo:<br> ${alumno.grupo}</p>
            <br>
        `;
        lista.appendChild(div);
    });
});


/*console.log ("Matricula"+ alumno.Matricula);
console.log("nombre" + alumno.nombre);

alumno.nombre = "Acosta Dias Maria"
console.log ("Nuevo Nombre : "+ alumno.nombre);*/


//objetos compuestos

let cuentaBanco = {
    "numero" : "10001",
    "banco" : "Banorte",

    cliente: {
        "nombre" : "Jose Lopez",
        "fechaNac" : "2020 - 01 - 01",
        "sexo": "M" },
        "saldo" : "10000",
}
console.log("Nombre : "+ cuentaBanco.cliente.nombre);
console.log("saldo : " + cuentaBanco.saldo);
cuentaBanco.cliente.sexo ="F";
console.log(cuentaBanco.cliente.sexo)

//arrelgo de productos

let productos = [{"codigo" : "1001",
"descripcion" : "Atun",
"precio" : "34"},{"codigo" : "1002",
"descripcion" : "jabon en polvo",
"precio" : "23"
},{"codigo" : "1003",
"descripcion" : "Harina",
"precio" : "43"},{"codigo" : "1004",
"descripcion" : "Pasta dental",
"precio" : "78",}]
//Mostrar un atributo de un objeto del arreglo
console.log ("La descripcion es" + productos[0].descripcion);

for(let i=0 ;i<productos.length;i++){
    console.log("codigo: " +productos[i].codigo);
    console.log ("descripcion" + productos[i].descripcion);
    console.log("precio"+ productos[i].precio);
}