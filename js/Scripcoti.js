    /* declarar variables */

const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click',function(){
    //obtener los datos de los input
    let valorAuto = document.getElementById('valorAuto').value;
    let pInicial = document.getElementById('Porcentaje').value;
    let plazos = document.getElementById('Plazos').value;

    //hacer calculos

    let pagoInicia = valorAuto *(pInicial/100);
    let totalFin = valorAuto-pagoInicia;
    let pagoMensual = totalFin/plazos;

    //mostrar los datos

    document.getElementById('pagoInicial').value = pagoInicia;
    document.getElementById('totalFin').value = totalFin;
    document.getElementById('pagoMensual').value = pagoMensual;
});  
