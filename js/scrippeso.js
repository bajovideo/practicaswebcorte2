document.getElementById("calcular").addEventListener("click", function() {
    var peso = parseFloat(document.getElementById("peso").value);
    var altura = parseFloat(document.getElementById("altura").value);
    
    var genero;
    var radioButtons = document.getElementsByName("genero");
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            genero = radioButtons[i].value;
            break; // Sal del bucle una vez que encuentres el radio button seleccionado
        }
    }

    var edad = parseInt(document.getElementById("edad").value);

    if (!isNaN(peso) && !isNaN(altura) && altura > 0 && !isNaN(edad)) {
        var imc = peso / (altura * altura);
        var resultado = "Su IMC es: " + imc.toFixed(2);

        document.getElementById("resultado").textContent = resultado;

        // Mostrar la imagen correspondiente al rango de IMC
        var imcImg = document.getElementById("imc-img");
        var imagenIMC = document.getElementById("imagen-imc");

        if (imc < 18.5) {
            imcImg.src = "/img/01.png";
        } else if (imc >= 18.5 && imc < 24.9) {
            imcImg.src = "/img/02.png";
        } else if (imc >= 24.9 && imc < 29.9) {
            imcImg.src = "/img/03.png";
        } else if (imc >= 29.9 && imc < 34.9) {
            imcImg.src = "/img/04.png";
        } else if (imc >= 34.9 && imc < 39.9) {
            imcImg.src = "/img/05.png";
        } else {
            imcImg.src = "/img/06.png";
        }

        imagenIMC.style.display = "block"; // Mostrar la imagen

        // Calcular y mostrar las calorías recomendadas
        var caloriasSpan = document.getElementById("calorias");
        var caloriasDiv = document.getElementById("calorias-recomendadas");
        var caloriasRecomendadas;

        // Estos son ejemplos de valores calóricos recomendados basados en género y edad
        var caloriasHombre = 2500; // Ejemplo para hombres
        var caloriasMujer = 2000;  // Ejemplo para mujeres

        if (genero === "hombre") {
            caloriasRecomendadas = caloriasHombre;
        } else {
            caloriasRecomendadas = caloriasMujer;
        }

        // Ajustar las calorías recomendadas según la edad
        if (edad >= 18 && edad <= 30) {
            caloriasRecomendadas += 200;
        } else if (edad > 30 && edad <= 50) {
            caloriasRecomendadas += 100;
        }

        caloriasSpan.textContent = "Calorías recomendadas: " + caloriasRecomendadas + " kcal/día";
        caloriasDiv.style.display = "block"; // Mostrar la información de calorías
    } else {
        document.getElementById("resultado").textContent = "Ingrese valores válidos.";
    }
});